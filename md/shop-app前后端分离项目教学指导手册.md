# 需求

# 流程列表

| 编号 | 流程               | 前端组件                      | 页面设计 | 后端模块            |
| ---- | ------------------ | ----------------------------- | -------- | ------------------- |
| 1    | 登录流程           | login.vue                     | 未完成   | UserServlet         |
| 2    | 注册流程           | mobliePhone.vue、Register.vue | 完成     | UserServlet         |
|      | 手机号确认         | MobilePhone.vue               | 完成     |                     |
|      | 输入密码           | Register.vue                  | 完成     |                     |
| 3    | 购物车商品数量获取 | TopA.vue                      | 完成     | ShoppingCartServlet |
| 4    | 购物车商品展示     | ShoppingCart                  | 完成     | ShoppingCartServlet |
| 5    | 购物车数据修改     | ShoopingCart                  | 完成     | ShoppingCartServlet |

# 流程描述

# 1，登录流程

## 前端部分

用户名和密码都使用双向绑定

```vue
<input type="text" v-model="userName" />
<input type="password" v-model="password" />
```

使用axios进行AJAX请求

```js
async loginOperation() {
    try {
        const response = await this.$axios.get("/api/v1/user/login", {
            params: {
                userName: this.userName,
                password: this.password
            }
        });
        if (response.data.result == "ok") {
            this.$router.push({ name: "Home" });
        } else {
            this.$alert("用户名或密码错误！", "提示", {
                confirmButtonText: "确定"
            });
        }
    } catch (error) {
        console.log(error);
    }
}
```

需要注意的问题是：

1. 此处使用了RestFul风格的请求格式，而且还涉及到了请求参数。
   - 请求格式统一为：`/api/v1/`，其后的user代表请求的控制器类，login代码类中具体的方法或处理流程。
2. 使用axios.get()函数需要带参数时，使用一个对象做为函数的第二个参数，其中params属性中包含各种参数对。
3. 这里还使用了ElementUI中的MessageBox.alert对象。

## 后端部分

使用基于maven的多模块开发技术，将web-server、service、dao、dbutils分隔在不同模块中。以利于大型应用系统的开发。

使用UserServlet进行相应的处理

映射了`@WebServlet("/api/v1/user/*")`URL，从而定位到相应的Servlet。

通过Servlet调用Service、并继续调用Dao、最终调用dbutils从而完成对数据库的操作。

# 2，注册流程

注册流程相对复杂一些。具体流程如下：

1. 页面会从后台获取国家编码，如果中国用户编码为：+86。
2. 然后用户首先提交手机号码。
3. 转向验证码页面（省略），如果验证码正确，则转向注册页面。
4. 在注册页中输入两次密码，点击提交，就可以成功注册相关用户。

## 前端部分

### axios.post()函数传参问题

由于是新增数据，按照Restful风格应该使用POST提交方法。在axios中使用POST方式提交并传值有一些问题。

语法：

```js
axios.post(url,{key:value,key:value});
```

这种写法传递是的JSON字符串，这时候后端就不能使用request.getParameter()方法来接收数据。

要么使用流操作接收，要么在前端修改传递方式。

1，前端修改传递方式

在前端使用URLSeachParams()函数，将数据追加进行该函数的实例中。

```js
let param = new URLSearchParams();
param.append("password", this.password);
param.append("phone", this.phone);
param.append("code", this.code);
let response = await this.$axios.post("/api/v1/user/register", param);
```

之函数较新，适配该函数的浏览器不多。目前看chrome和firefox的最新版本都可以。

2，后端使用流操作，获取JSON字符串，再利用JSON字符串将其置换成Java对象。

```java
StringBuffer sb = new StringBuffer("");
BufferedReader br = new BufferedReader(new InputStreamReader(
    (ServletInputStream) request.getInputStream(), "utf-8"));
String temp;
while ((temp = br.readLine()) != null) {
    sb.append(temp);
}
br.close();
String json = sb.toString();
JSONObject jsonObject = JSON.parseObject(json);	//转换成JSONObject对象

String phone = jsonObject.getString("phone");
String password = jsonObject.getString("password");
String code = jsonObject.getString("code");
```

## 后端部分

后端使用doPost()函数处理相应的请求。请求格式：/api/v1/user/register

# 3，购物车内商品数量的获取

## 详细需求

1. 需要获取用户信息中的用户id。因为所有的和购物车数据表相关的操作，本质上都是针对某个具体人的。所以必须要有这个人的id才可以操作。
2. 向后端传递用户id，并把后端传回的当前用户所选择的购物车中的商品总数。

## 1，获取并在多个组件中保持用户信息

### 获取用户信息

在Login.vue中，执行登录操作时，不仅仅返回登录时否成功。在登录成功后，还要返回一个完整的用户信息，其中就包含用户id。

在后端，将原来登录SQL进行修改即可，完成相应的操作：

```mysql
-- 原来的SQL语句
select count(*) from user where name=? and password=?;
-- 为了可以返回用户信息而修改的SQL语句
select * from user where name =? and password =?;
```

后端获取了用户信息后，以JSON字符的形式返回给前端。

### 在多个组件中保持用户信息

使用Vuex插件，可以在多个组件中保持用户信息。只需要在main.js中注册Vuex插件，并组件Vuex实例进行操作即可。

利用Mutation将数据提交到Vuex中的state属性中。需要注意的是：state属性中的数据只能通过Mutations来提交，不能直接修改。

#### 提交数据

1，在vuex中的Mutations属性中，设置一个修改函数，该函数的第一个参数是state本身，第两个参数是载荷（payload)，即要提交上来的数据。

```js
state: {
    user:null,		//提前定义了user，在没有获得数据的时候为null
},
mutations: {
   setUser(state,user){
       state.user = user;
   }
}
```

解析：存放数据的属性是state属性，该属性可以直接读取，但不能直接修改。

2，该函数必须在组件中调用才有意义，调用前需要先将该函数混入到当前组件的methods属性中。使用mapMutation。

```js
import {mapMutations} from 'vuex';		//导入相关函数
...
methods: {
    //使用展开运算符混入setUser()函数
    ...mapMutations([
        'setUser'
    ]),
    loginOperation(){
        ...
        //混入后，可以像使用本函数一样使用在vuex中定义的setUser()函数了。
    	this.setUser(user);	//其中的参数，就是定义setUser()函数时的第二个参数。
    }
}
```

#### 读取数据

各个组件都可以读取vuex中的数据，可以直接使用`this.$store.state.user`的方式，也可以利用mapState()函数将state中的数据混入当前组件中。

```js
computed:{
	...mapState({
		user:'user'		//也可以：user:state=>state.user
	})
}
```

此时，就可以像使用本地数据一样，使用vuex中的user数据了。如下：

```vue
<div slot="userInfo" style="margin-right:15px">{{user.name}} &nbsp;&nbsp; | 我的订单</div>
```

## 2，发送一次get方式的请求

由于是要获取一个值，按照RestFul的风格，使用get方法提交请求。

```js
async initSoppingCart() {
    let response = await this.$axios.get("/api/v1/shoppingCart/num", {
        params: { id: this.user.id }
    });
    this.goodsNumber = response.data.number;
}
```

和axios.put()、axios.post()函数不同，axios.get()函数的第二参数，虽然也是一个对象，但其中只能有一个键值对，即：`params:value`。多个数据需要传递时，直接把多个键值对写在对象中，如下：

```js
this.$axios.get("/api/v1/shoppingCart/num", {
        params: { 
            id: this.user.id,
            name:this.user.name 
        }
    });
```

axios.get()函数的这种参数提交方式，在后端可以直接使用`request.getParameter("id")`方法获取对应的值。

# 4，购物车商品的展示

## 详细需求

获取购物车数据表中和某个用户相关连的所有数据

1. 由于某些商品包含了赠品信息，所以需要在业务中提交对赠品信息进行业务处理，主要的工作是赠品数据放入对应的商品元素中。
2. 这里涉及到了数据的设计问题和后端domain的设计问题，以及具体的业务处理问题。

### 1，前端处理

由于之前使用了vue.config.js配置，进行了数据的模拟操作，所以前端基本上不需要做较大的修改，一般的修改集中数据中的命名方式上，比如：将imageUrl改成按照MySQL习惯使用的image_url。

### 2，后端处理

#### 数据设计

```mysql
create table shopping_cart
(
   id                   int not null auto_increment,
   goods_id             int comment '对应的商品',
   user_id              int comment '对应的用户',
   number               int not null default 1 comment '商品数量，默认为1',
   total_price          decimal not null comment '商品价格*数量',
   parent_id            int default 0 comment '礼品为id，非礼品为0，指定是哪个商品的礼品，默认值为0，为0时不是礼品。',
   primary key (id)
);
```

主要的设计体现在parent_id字段上，该字段承载了两个任务：1，确定某个商品是不是赠品；2，如果是赠品，则可以进一步确定该赠品是哪个商品的赠品。

当一个商品是赠品时，parent_id的值对应的就是购物车表中的id值。

### domain的设计

```java
public class ShoppingCart {
    int id;
    int goodsId;
    int UserId;
    int number;
    BigDecimal totalPrice;
    int parentId;
    List<Map<String,Object>> gifts = null;
    boolean selected = true;
}
```

其中有两个字段不是数据库中的字段：gift和selected。其中gifts是用来存放当前商品的赠品的；selected是用来存入是否被选中的，这两个字段都是和数据库无关的。

> 深入思考：实际上selected属性也是可以放到shopping_cart数据表中的。

### 具体业务的设计

具体代码如下：

```java
public List<Map<String,Object>> getList(int userId) {
    List<Map<String,Object>> list = shoppingCartDao.getShoppingCartByUserId(userId,true);   //获取用户选中的商品（不包含赠品）
    list.forEach(goods->{
        goods.put("selected",true);
    });
    List<Map<String,Object>> gifts = shoppingCartDao.getGifts(userId);
    if(null!=gifts && !gifts.isEmpty()){
        gifts.forEach(gift->{
            var id = gift.get("parent_id");
            List<Map<String,Object>> tempList =list.stream().filter(goods->goods.get("id")==id).collect(Collectors.toList());//赠品归属的商品
            if(null!=tempList && !tempList.isEmpty()) {
                log.debug(tempList.toString());
                tempList.get(0).put("gifts", new ArrayList<Map<String, Object>>());
                ((List<Map<String, Object>>) tempList.get(0).get("gifts")).add(gift);
            }
        });
    }
    return list;
}
```

大致的简单描述：同时提取两组数据：一组是全是商品的数据，另一组是全是赠品的数据。然后两组数据之间相互操作，从而将赠品放入商品的gifts属性中。

### 课堂作业

描述以上的业务代码的具体作用，而实际的效果。明天提交。提交方式：以你的名称命名的txt文本。

# 5，购物车页面中的数据修改

## 详细需求

购物车中需要同步的数据库中的数据有：

1. 商品的购物数量，购物数量变化会使用商品小计联动变化。
2. 删除商品
3. 商品的选择（购物车行首的复选框）

## 1，商品的购物数据

### 前端监听购物车中数据的变化

使用watch属性，监听购物车中已绑定数据的变化，这里就是监听list数组。由于是一个复杂对象。所以需要加`deep:true`，从而要求对该对象内部的数据变化也进行监听。

```js
watch: {
    list: {
        async handler(newValue) {
 			...
        },
        deep: true
    }
}
```

其中的handler()函数是当数据发生变化时，将被执行的函数，其听可以有两个参数：第一个是新的数据，第二个是变化前的老数据。这里的newVaule就是变化后的list数组。

### 前端向后端提交请求

这里涉及到向后端提交更新的数据，所以按照RestFul风格，需要使用PUT方式提交。提交代码如下：

```js
let response = await this.$axios.put("/api/v1/shoppingCart", {
    jsonString: JSON.stringify(newValue)
});
if (response.data.result === "ok") {
    this.$message({
        message: "购物车数据提交成功",
        type: "success"
    });
} else {
    this.$message({
        message: "购物车数据提交失败",
        type: "warning"
    });
}
```

使用axios按照PUT方式提交时，第二个参数是一个对象。最终提交给后端的是一个JSON字符串。所以需要使用`JSON.stringify()`函数将JS对象转换成JSON字符串。

后端部分

### 在Servlet中接收JSON字符串

利用axios.post()和axios.put()函数，带参数提交到后端的数据，都是JSON字符串，这些数据不能通过request.getParameter()方法直接拿到。

只能通过对ServletInputStream的操作才可以获取。可以将该操作重构成一个工具类中的静态方法，具体如下：

```java
public static String getJson(ServletInputStream sis){
    StringBuffer sb = new StringBuffer("");
    try {
        BufferedReader br = new BufferedReader(new InputStreamReader(sis, "utf-8"));
        String temp;
        while ((temp = br.readLine()) != null) {
            sb.append(temp);
        }
        br.close();
    } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
    return sb.toString();
}
```

该静态方法可以使用，如下方式调用：

```java
//使用前端传过来的inputStream流程作为参数
String jsonString = WebUtils.getJson(req.getInputStream()); 
```

解析：该方法返回的字符串，就是前端传递过来的JSON字符串。

注意：如果使用后端框架，如：Spring MVC等，则可以免去相关的操作。

### 使用FastJson对JSON字符串进行转换

FastJson是阿里巴巴开源的Java对象与JSON转换的工具。

把JSON字符串转换成Java对象的具体过程。

首先，在该流程中，传过来的是一个键值对：

```js
{
    jsonString: JSON.stringify(newValue)
}
```

使用JSON.praseObject()方法会将这个键值对对应的字符串，转换成JSONObject。而JSONObject是FastJson提供的Java对象，该对象提供了一般类型数据的提取方法，比如：getString(key)，getBigDecimal(key)。

该流程中前端的newValue是一个复杂的数组，数组内部每个元素都是一个对象，对象中又有复杂的键值对。这类复杂的数据，需要层层拆解，具体如下：

第一步，先把最外层的对象拆开，拿到数组本身。这个数组也是FastJson提供的：JSONArray。该类也实现了List接口，是一个类ArrayList的实现。默认情况下，该集合内部存放的是Object对象。

```java
JSONArray json = JSONObject.parseObject(jsonString).getJSONArray("jsonString");
//以上代码是缩写，其完整的写法如下：
JSONObject jsonObject = JSONOjbect.parseObject(jsonstring);	//jsonOjbect中只有一个键值对
JSONArray json = jsonOjbect.getJSONArray("jsonString");	//拆开JSONObject对象，取出集合
```

第二步，可以直接使用JSONArray在层间传递数据，一般在Servlet中将JSONArray或类似的数据传递给Service。由Service进一步拆解。

```json
//这里的arrays就是JSONArray类的实例，即上面代码中的json
arrays.forEach(item->{
    //把JSONArray集合中的元素强制转换成对应的对象或集合
    Map<String,Object> goods = (Map<String,Object>)item;	
	int rowNumber = shoppingCartDao.update(goods);	//一般性操作
	sum[0]++;
});
```

## 2，更新购物车中的商品

本质上来说所谓删除商品，在大部分情况下，是删除+插入操作。即删除当前用户所有的购物车中的商品，把newValue中的商品插入到购物车中。

由于前面使用watch监听了购物车list，所以当删除购物车中的某个商品时，也会触发watch中的handler函数。

由于购物车中数据的变化是不可控的，所以将购物车数据表中的数据进行简单的更新不可能完成全部任务的。

使用watch监听list，会导致list在首次初始化时就被修改了。

### 不能删除oldValue的数据

一开始，我们使用错误的设计思路：handler()中带有新数据和老数据，我们利用老数据删掉数据库的相关元素；再利用新数据插入这些元素，但这是不必要的，还会产生各种问题。

因为我们的设计思想应该是：删除掉某个用户的所有的购物车数据，也就是说，其实不需要老数据。只需要用户id号即可。

### 插入数据的时候，一块插入某个商品的赠品

### 完整的操作代码：

```java
public boolean updataShoppingCart(JSONArray newArrays,int userId) {
    boolean flag = true;
    List<Map<String,Object>> list = new ArrayList();
    //笨办法的类型转换
    newArrays.forEach(object->{
        Map<String, Object> map = (Map<String, Object>) object;
        list.add(map);
    });
    try {
        TranscationManager.beginTranscation();
        int deleteRow = 0;
        deleteRow = shoppingCartDao.delete(userId);		//删除当前用户所有的购物车中的数据
        List<Map<String,Object>> gifts = new ArrayList(); //初始化礼物列表
        //找出所有赠品的商品的集合
        var tempList = list.stream().filter(map->null!=map.get("gifts") && !((List<Map<String,Object>>)map.get("gifts")).isEmpty()).collect(Collectors.toList());
        //将有赠品的商品中的所有的赠品数据，放放入gifts集合中
        tempList.forEach(map->gifts.addAll((List<Map<String,Object>>)map.get("gifts")));
        //把赠品的集合，合并到商品集合中，此时在商品集合中，商品元素和赠品元素是同级的。
        list.addAll(gifts);
        int sum[] = {0};
        list.forEach(map -> {
            shoppingCartDao.insert(map);
            sum[0]++;
        });
        TranscationManager.commit();
    } catch (TranscationException e) {
        TranscationManager.rollback();
        e.printStackTrace();
    }
    return flag;
}
```

