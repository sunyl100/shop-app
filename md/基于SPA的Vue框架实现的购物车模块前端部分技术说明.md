# 模仿小米商城的购物车模块

# 需要讲解的技术问题

## 用Vue绑定技术绑定图片

```js
data(){
	return{
        imageUrl:require(图片相对地址);
    }
}
```

此时这个图片应该位于public目录，不是位于assets目录中。

- public目录中的图片是静态的，webpack不会编译public中的图片。
- assets目录中的图片在编译时，会被转换成二进制直接放在js文件中。

## SCSS在当前项目中的应用

使用了SCSS技术，该技术完全兼容CSS，同时为CSS增加和很多功能，我们在该项目中使用了：

变量，在SCSS中可以声明变量，所有变量都带有$前缀的字符，可以给变量赋值；并在之后使用这些变量。

```scss
$content-width: 65%; /*声明一个变量，其中存放内容宽度*/
.header-content {
    height: 100px;
    width: $content-width;	/*使用变量*/
    margin: 0 auto;
}
```

嵌套，SCSS支持属性的嵌套使用

```scss
.header {
  height: 100px;
  width: 100vw;
  .header-content {
    height: 100px;
    width: $content-width;
    margin: 0 auto;
    .logo {
      width: 50px;
    }
  }
}
```

以上代码如果用CSS来写，则应该为：

```css
.header {
  height: 100px;
  width: 100vw;
}
.header>.header-content {
    height: 100px;
    width: $content-width;
    margin: 0 auto;
}
.header>.header-content .logo {
    width: 50px;
}
```

## 总价钱、商品个数、已选择的商品个数

这三个数据由于都是会被多个数据的变化而影响的，所以这三个数据都放在computed属性中。

```js
totalPrice() {
    let sum = 0;
    sum = this.list
        .filter(goods => goods.selected)
        .reduce((pre, goods) => pre + goods.number * goods.price, 0);
    return sum;
}
```

## 顶部状态栏组件化

把和顶部状态栏相关的模板和CSS代码剪切到Top.vue文件中。在Top.vue中定义props属性，其中的键值对分别为：

```js
props:{
	title:String,
    subTitle:String
}
```

然后在ShoppingCart.vue中导入Top.vue并且注册，然后直接然Top.vue当成自定义组件使用。并且分别给Top标签中的属性title和subTitle赋值。

```vue
<Top title="我的购物车" subTitle="温馨提示：产品是否购买成功，以最终下单为准哦。请尽快结算"></Top>
```

## CSS中的height:100vh问题

一般情况下，不建议在PC端使用100vh，也建议使用100vw。

其在PC端可能造成的问题有：

1，出现不必要的滚动条。

2，当内容较多，必须出现垂直滚动条时，出现视口之外的部分没有背景渲染的情况。

同样，当出现水平滚动条时，在视口外部的背景也无法被渲染，这不是100vw导致的，而是浏览器本身的问题，解决方案是：

```css
min-width: max-content; /*指定渲染时，安最大元素的宽度渲染*/
```



# 尚未解决的问题

## 结算信息位置处理

当购物车的高度超过了视口的高度时，结算信息位于视口的最底部，并且浮动在HTML文档标准流的上面。

当滚动条滚动时，随着购物车的内容全部出现，结算信息回到购物车底部（不再位于视口的最底部）。

具体实现技术

获取视口的高度，和结算信息所处位置的高度。如果视口不足以显示结算信息，则结算信息设置为fixed。

不断监听滚动条的变化，当视口的高度+滚动条顶部距离初始位置的高度大于结算信息所处位置时，恢复结算信息原来的特性。

业务代码：

```js
mounted() {
    let element = 100+70+50+document.querySelector(".shopping-cart__table").offsetHeight; 
    let viewPort = document.body.clientHeight;                      //视口的高度
    console.log(element,viewPort);
    if (element <= viewPort) {
         document.querySelector(".shopping-cart__acount").classList.add("isFixed");
    }
    window.addEventListener("scroll", this.handleScroll);   //监听滚动条滚动事件
},
methods: {
    handleScroll() {
       let element = 100+70+50+document.querySelector(".shopping-cart__table").offsetHeight;
       let scrollTop =window.pageYOffset ||
            document.documentElement.scrollTop ||
            document.body.scrollTop;
       let viewPort = document.body.clientHeight;
       if (element <= scrollTop + viewPort) {
			document.querySelector(".shopping-cart__acount")
			.classList.remove("isFixed");
		} else {
			document.querySelector(".shopping-cart__acount")
			.classList.add("isFixed");
		}
	}
}
```

css代码：

```css
.isFixed {
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 999;
  width: $content-width;
  margin: 0 auto;
  box-shadow: 0 -5px 5px #f5f5f5;
}
```

还没有解决的问题：

1，结算信息的位置没有精确的确定。

2，视口可能出现没有刷新的问题。