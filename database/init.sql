create database shop;


-- 用户表
create table user
(
   id                   int not null,
   name                 varchar(50),
   password             varchar(50),
   mobile_phone         varchar(20),
   country_id           varchar(5) default '+86' comment '对应国家字典表中的id',
   primary key (id)
);
alter table user add constraint FK_Reference_1 foreign key (country_id)
      references country (id) on delete restrict on update restrict;
	  
insert into user(name,password,phone) values('admin','admin','13498708765'),('manager','manager','12398767654');



-- 商品相关数据表
create table goods
(
   id                   int not null auto_increment,
   name                 varchar(100),
   price                decimal(6,2) comment '该类型商品的最低价格',
   primary key (id)
);
alter table goods comment '商品表，描述商品的基本性质。商品名称和商品的最低价格。';

select * from goods g inner join goods_image gi on g.id=gi.user_id;		--查询商品

-- 商品中的图片
create table goods_image
(
   id                   int not null auto_increment,
   image_url            varchar(300) comment 'url地址',
   image_type           char(1) default '0' comment '0为普通图片，1为图标图图片',
   goods_id             int comment '商品id',
   primary key (id)
);

alter table goods_image comment '用来存放对应商品的图片';

alter table goods_image add constraint FK_Reference_2 foreign key (user_id)
      references goods (id) on delete restrict on update restrict;

-- 购物车
create table shopping_cart
(
   id                   int not null auto_increment,
   goods_id             int comment '对应的商品',
   user_id              int comment '对应的用户',
   number               int not null default 1 comment '商品数量，默认为1',
   total_price          decimal not null comment '商品价格*数量',
   parent_id            int default 0 comment '礼品为id，非礼品为0，指定是哪个商品的礼品，默认值为0，为0时不是礼品。',
   primary key (id)
);
alter table shoppingCart add constraint FK_Reference_3 foreign key (goods_id)
      references goods (id) on delete restrict on update restrict;
alter table shoppingCart add constraint FK_Reference_4 foreign key (user_id)
      references user (id) on delete restrict on update restrict;
	  
	
select * from shoppingCart where user_id = 1;		-- 查询id为1的用户的购物车信息	

-- 获取购物车中的商品名称，商品价格等数据
select sc.id,sc.goods_id,sc.user_id,sc.number,sc.total_price,g.name,g.price from shopping_cart sc
 inner join goods g on sc.goods_id = g.id
 where sc.user_id = 1;
	  
-- 获取购物车中的完整数据
select sc.id,sc.goods_id,sc.user_id,sc.number,sc.total_price,g.name,g.price,gi.image_url from shopping_cart sc
 inner join goods g on sc.goods_id = g.id
 inner join goods_image gi on sc.goods_id = gi.goods_id 
 where sc.user_id = 1 and gi.image_type=0;
 
-- 获取购物车中的赠品
select sc.id,sc.goods_id,sc.user_id,sc.number,sc.total_price,g.name,g.price,gi.image_url from shopping_cart sc
 inner join goods g on sc.goods_id = g.id
 inner join goods_image gi on sc.goods_id = gi.goods_id 
 where sc.user_id = 1 and gi.image_type=0 and sc.parent_id != 0;  
	  
	  
-- 字典表，国家信息编码
create table country
(
   id                   int not null auto_increment,
   name                 varchar(50) not null,
   code                 varchar(5) not null,
   selected             char default '0',
   primary key (id)
);
alter table country comment '字典表，存放国家手机信息编码';